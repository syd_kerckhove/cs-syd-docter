{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module CsSyd.Docter.Types where

import Data.Aeson
import Data.Text (Text)
import qualified Data.Text as T
import Data.Time
import Data.Validity
import Data.Validity.Path ()
import Data.Validity.Text ()
import GHC.Generics (Generic)
import Path
import Text.Printf

data OutputDirective
  = ToStdOut
  | ToFile (Path Abs File)
  deriving (Show, Eq, Generic)

instance Validity OutputDirective

data PhoneNumber =
  PhoneNumber
    { phoneCountryCode :: Text
    , phoneNumber :: Text
    }
  deriving (Show, Eq, Generic)

instance Validity PhoneNumber

instance FromJSON PhoneNumber where
  parseJSON =
    withObject "PhoneNumber" $ \o ->
      PhoneNumber <$> o .: "country code" <*> o .: "number"

instance ToJSON PhoneNumber where
  toJSON PhoneNumber {..} =
    object ["country code" .= phoneCountryCode, "number" .= phoneNumber]

instance ToJSON (ForMustache PhoneNumber) where
  toJSON (ForMustache PhoneNumber {..}) =
    object
      [ "country-code" .= phoneCountryCode
      , "number" .= phoneNumber
      , "full-number" .= T.unwords [phoneCountryCode, phoneNumber]
      ]

emptyPhoneNumber :: PhoneNumber
emptyPhoneNumber = PhoneNumber {phoneCountryCode = "", phoneNumber = ""}

data Address =
  Address
    { addressName :: Text
    , addressCompany :: Text
    , addressStreet :: Text
    , addressNumber :: Text
    , addressPostCode :: Text
    , addressCity :: Text
    , addressCountry :: Text
    }
  deriving (Show, Eq, Generic)

instance Validity Address

instance FromJSON Address where
  parseJSON =
    withObject "Address" $ \o ->
      Address <$> o .: "name" <*> o .: "company" <*> o .: "street" <*>
      o .: "number" <*>
      o .: "post code" <*>
      o .: "city" <*>
      o .: "country"

instance ToJSON Address where
  toJSON Address {..} =
    object
      [ "name" .= addressName
      , "company" .= addressCompany
      , "street" .= addressStreet
      , "number" .= addressNumber
      , "post code" .= addressPostCode
      , "city" .= addressCity
      , "country" .= addressCountry
      ]

instance ToJSON (ForMustache Address) where
  toJSON (ForMustache Address {..}) =
    object
      [ "name" .= addressName
      , "company" .= addressCompany
      , "street" .= addressStreet
      , "number" .= addressNumber
      , "post-code" .= addressPostCode
      , "city" .= addressCity
      , "country" .= addressCountry
      ]

emptyAddress :: Address
emptyAddress =
  Address
    { addressName = ""
    , addressCompany = ""
    , addressStreet = ""
    , addressNumber = ""
    , addressPostCode = ""
    , addressCity = ""
    , addressCountry = ""
    }

newtype EmailAddress =
  EmailAddress Text
  deriving (Show, Eq, Generic)

instance Validity EmailAddress

instance FromJSON EmailAddress where
  parseJSON o = EmailAddress <$> parseJSON o

instance ToJSON EmailAddress where
  toJSON (EmailAddress t) = toJSON t

instance ToJSON (ForMustache EmailAddress) where
  toJSON (ForMustache (EmailAddress t)) = String t

emptyEmailAddress :: EmailAddress
emptyEmailAddress = EmailAddress ""

data PaymentDetails =
  PaymentDetails
    { paymentBeneficiary :: Text
    , paymentIban :: IBAN
    , paymentBic :: Text
    }
  deriving (Show, Eq, Generic)

instance Validity PaymentDetails

instance FromJSON PaymentDetails where
  parseJSON =
    withObject "PaymentDetails" $ \o ->
      PaymentDetails <$> o .: "beneficiary" <*> o .: "iban" <*> o .: "bic"

instance ToJSON PaymentDetails where
  toJSON PaymentDetails {..} =
    object
      [ "beneficiary" .= paymentBeneficiary
      , "iban" .= paymentIban
      , "bic" .= paymentBic
      ]

instance ToJSON (ForMustache PaymentDetails) where
  toJSON (ForMustache PaymentDetails {..}) =
    object
      [ "beneficiary" .= paymentBeneficiary
      , "iban" .= ForMustache paymentIban
      , "bic" .= paymentBic
      ]

newtype IBAN =
  IBAN Text
  deriving (Show, Eq, Generic)

instance Validity IBAN

instance FromJSON IBAN where
  parseJSON o = IBAN <$> parseJSON o

instance ToJSON IBAN where
  toJSON (IBAN t) = toJSON t

instance ToJSON (ForMustache IBAN) where
  toJSON (ForMustache (IBAN t)) =
    object ["strict" .= t, "by4" .= T.unwords (T.chunksOf 4 t)]

data Price =
  Price -- Use Hledger 'Amount'
    { priceQuantity :: Double
    , priceSign :: Text
    }
  deriving (Show, Eq, Generic)

instance Validity Price where
  validate p@Price {..} =
    mconcat
      [ genericValidate p
      , validateNotNaN priceQuantity
      , validateNotInfinite priceQuantity
      ]

instance FromJSON Price where
  parseJSON =
    withObject "Price" $ \o -> Price <$> o .: "quantity" <*> o .: "sign"

instance ToJSON Price where
  toJSON Price {..} = object ["quantity" .= priceQuantity, "sign" .= priceSign]

instance ToJSON (ForMustache Price) where
  toJSON (ForMustache Price {..}) =
    object
      ["full" .= T.unwords [T.pack $ printf "%.2f" priceQuantity, priceSign]]

priceMult :: Price -> Double -> Price
priceMult p d = p {priceQuantity = priceQuantity p * d}

-- this does not check whether the signs mtch
priceAdds :: [Price] -> Price
priceAdds [] = Price 0 ""
priceAdds ps = Price (sum $ map priceQuantity ps) $ priceSign $ head ps

newtype ForMustache a =
  ForMustache a
  deriving (Show, Eq, Generic)

instance ToJSON (ForMustache a) => ToJSON (ForMustache (Maybe a)) where
  toJSON (ForMustache Nothing) = Bool False
  toJSON (ForMustache (Just a)) = toJSON $ ForMustache a

instance ToJSON (ForMustache Text) where
  toJSON (ForMustache t) = toJSON t

instance ToJSON (ForMustache Day) where
  toJSON (ForMustache d) =
    object
      [ "strict" .= formatTime defaultTimeLocale "%Y-%m-%d" d
      , "full" .= formatTime defaultTimeLocale "%A %B %e %Y" d
      ]
