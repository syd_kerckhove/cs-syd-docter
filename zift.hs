#!/usr/bin/env stack
{- stack
    --install-ghc
    runghc
    --package zifterlib
-}
import CsSyd.Zifterlib

main :: IO ()
main = ziftWith csSydHaskellProject
